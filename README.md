# Search Engine Party

[![Website](https://img.shields.io/website?url=https%3A%2F%2Fsearchengine.party)](https://searchengine.party)
[![Netlify Status](https://api.netlify.com/api/v1/badges/f439eaff-bfa0-45bb-9285-99a14cb4125e/deploy-status)](https://app.netlify.com/sites/searchengines/deploys)
[![License](https://img.shields.io/badge/licensed-ethically-%234baaaa)](https://firstdonoharm.dev/)
[![Mastodon Follow](https://img.shields.io/mastodon/follow/1?domain=https%3A%2F%2Fnitro.horse&style=social)](https://nitro.horse/@andreas)

https://searchengine.party/

A reference table for comparing popular private and non-private search engines across security and privacy-related data points.

## HTML Template

```html
<tr>
	<td id="example-name"><a href="https://example.com" target="_blank" rel="external noopener noreferrer">Example</a></td>
	<td id="example-since">Year Established</td>
	<td id="example-type">For-Profit | Non-Profit | Informal Collective</td>
	<td id="example-jurisdiction">Country Code</td>
	<td id="example-ssllabs" class="a-rating"><a href="https://www.ssllabs.com/ssltest/analyze.html?d=example.com" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">A<br /></div></a></td>
	<td id="example-observatory" class="a-rating"><a href="https://observatory.mozilla.org/analyze/example.com" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">A<br /></div></a></td>
	<td id="example-ip-logging" class="a-rating"><a href="https://example.com/privacy" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">N<br /></div></a></td>
	<td id="example-ip-sharing" class="a-rating text-rating">N</td>
	<td id="example-3rd-party-trackers" class="a-rating text-rating">N</td>
	<td id="example-onion-service" class="a-rating"><a href="https://example.onion/" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">Y<br /></div></a></td>
	<td id="example-post-request" class="a-rating text-rating">Y</td>
	<td id="example-dnssec" class="a-rating"><a href="https://en.internet.nl/site/example.com" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">Y<br /></div></a></td>
	<td id="example-ipv6" class="a-rating"><a href="https://en.internet.nl/site/example.com" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">Y<br /></div></a></td>
	<td id="example-hsts" class="a-rating"><a href="https://en.internet.nl/site/example.com" target="_blank" rel="external noopener noreferrer" class="text-rating"><div class="a-rating text-rating">Y<br /></div></a></td>
	<td id="example-disabled-js" class="a-rating text-rating">Y</td>
	<td id="example-onion-captchas" class="a-rating text-rating">N</td>
	<td id="example-filters-adult-content" class="a-rating text-rating">N</td>
	<td id="example-proxy-service" class="a-rating text-rating">Y</td>
	<td id="example-clearnet-url">https://example.com/search?q=%s</td>
</tr>
```